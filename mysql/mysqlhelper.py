#!/usr/bin/python
# -*- coding: utf-8 -*-

""" 
-------------------------------------------------
@version    : v1.0 
@author     : fangzheng
@contact    : zfang@hillinsight.com
@software   : PyCharm 
@filename   : mysqlhelper.py
@create time: 2019/3/26 17:35 
@describe   : 
@use example: python mysqlhelper.py [param1 param2]
-------------------------------------------------
"""

from IDBHelper import DBHelper
from logger import Logger
import pymysql
import traceback
from dbutils import select,execute,DBConfig

log = Logger(name='mysqlhelper')

class MySQLHelper(DBHelper):
    """
    MySQLHelper
    """
    _connect = None

    def __init__(self, db_config):
        """
        Construnctor for MySQLHelper
        """
        self.init(db_config)

    def init(self, db_config):
        """
        init database connection
        :param dbconfig:
        :return: True/False
        """
        try:
            self.dbconfig = DBConfig(db_config)
            self._connect = pymysql.Connect(
                host=str(self.dbconfig.host),
                port=self.dbconfig.port,
                user=str(self.dbconfig.username),
                passwd=str(self.dbconfig.password),
                db=str(self.dbconfig.db),
                charset=str(self.dbconfig.charset)
            )
            log.info(" Connected to MySQL database [ {db} ]...".format(db=self.dbconfig.db))
            return True
        except Exception as e:
            log.error(" Connect MySQL exception : \n{e}\n".format(e=str(e)))
            return False

    def get_conn(self):
        if self._connect:
            return self._connect
        else:
            self.init()
            return self._connect

    def close_conn(self):
        if self._connect:
            self._connect.close()
            log.info(" MySQL database [ {db} ] connection closed....".format(db=self.dbconfig.db))


    def table_is_exist(self,table_name):
        """
        Check table is exist
        :param tablename:
        :return:
        """
        sql = "SHOW TABLES LIKE '{tname}'".format(tname=table_name)
        rows = self.select(sql=sql)
        if len(rows) >= 1:
            return True
        else:
            return False

    def select(self, sql, param=None , size=None):
        """
        Query data
        :param sql:
        :param param:
        :param size: Number of rows of data you want to return
        :return:
        """
        cur = self._connect.cursor(cursor=pymysql.cursors.DictCursor)
        rows = None
        try:
            cur.execute(sql, param)
            if size:
                rows = cur.fetchmany(size)
            else:
                rows = cur.fetchall()
        except Exception as e:
            self._connect.rollback()
            log.error(traceback.format_exc())
            log.error("[sql]:{} [param]:{}".format(sql, param))
        cur.close()
        return rows

    def execute(self,sql, param=None):
        """
        exec DML：INSERT、UPDATE、DELETE
        :param sql: dml sql
        :param param: string|list
        :return: Number of rows affected
        """
        return execute(self._connect, sql, param)
