# python-dbhelper

#### 介绍
Python数据库帮助类（支持Hive、MySQL、Oracle、SqlServer）

#### 软件架构
Python版本：Python3.7


#### 安装教程


1. `git clone https://gitee.com/fangzheng0518/python-dbhelper.git`
2. `cd python-dbhelper`
3. `pip install -r requirements.txt`
4. `python3 setup.py install`

注意：

如果第3步报错： ImportError: No module named Cython.Distutils

则需要先安装Cython
安装命令`pip install Cython`
然后再继续执行第3步`pip install -r requirements.txt`



#### API使用说明

##### 1. helper.select(sql,param=None,size=None)

**方法参数：**
1. sql : 查询的SQL语句，参数使用%s占位
2. param: SQL的参数
3. size: 期望返回的数据条数, 为空则返回所有数据, 例如：返回前10条(size=10)
**返回结果：**
  返回SQL查询的结果

##### 2. helper.execute(sql,param=None)

**方法参数：**

1. sql : 查询的SQL语句，参数使用%s占位
2. param: SQL的参数

**返回结果：**
  返回DML影响的行数

```sql
sql = 'DELETE TABLE user where id=1'
users = helper.execute(sql=sql)
```

#### 示例

```python

# 1、MySQL
from dbhelper.mysql.mysqlhelper import MySQLHelper
dbconfg = {
    'host':'127.0.0.1',
    'port':3306,
    'username':'root',
    'password':'****',
    'db':'test',
    'charset':'utf8'
}
helper = MySQLHelper(dbconfg)

# 判断表是否存在
print(helper.table_is_exist('tablename'))

# 不带参数查询
sql = "select * from user"
users = helper.select(sql=sql)
print(users)

# 不带参数查询前10条
sql = "select * from user"
users = helper.select(sql=sql,size=10)
print(users)

# 带参数查询
sql = "select * from user where id=%s"
users = helper.select(sql=sql,param=(1,))
print(users)

# DML语句：INSERT UPDATE DELETE
sql = "delete from user where id=10"
# sql = "INSERT INTO user (username , password , email , sal) VALUES ('test','分钟都干啥的','test@163.com',5000);"
users = helper.execute(sql=sql)
print(users)



# 2、Hive
from dbhelper.hive.hivehelper import HiveHelper
dbconfg = {
    'host': '127.0.0.1',
    'port': 10000,
    'db':'test',
    'username': 'test'
}
helper = HiveHelper(dbconfg)
sql = "select * from user" # Hive sql结尾不能有;分号
users = helper.select(sql=sql)
print(users)


# 3、SqlServer
from dbhelper.sqlserver.sqlserverhelper import SqlServerHelper
dbconfg = {
    'host': '127.0.0.1',
    'port': 2121,
    'db':'test',
    'username': 'test',
    'password': 'test'
}
helper = SqlServerHelper(dbconfg)
sql = "select * from user"
users = helper.select(sql=sql)
print(users)


# 4、Oracle
from dbhelper.oracle.oraclehelper import OracleHelper
dbconfg = {
    'host': '127.0.0.1',
    'port': 1521,
    'db':'orcl',
    'username': 'test',
    'password': 'test'
}
helper = OracleHelper(dbconfg)
sql = "select * from user"
users = helper.select(sql=sql)
print(users)

```


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request